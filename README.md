Lab 1: Transacting and Mining in an On-Campus Ethereum Network
===

In this lab, you are given the initial state that a custom Blockchain network of several miners is hosted on an on-campus machine which has been running for several days before the class. The Blockchain machine also runs a daemon that periodically instructs some miner to conduct transactions with other miners.

Lab Environment Setup
---

### 1. Install Ethereum on your OS

This step is to install Ethereum on ubuntu OS.

We will use `Geth`, the Ethereum client implemented in Language `Go`. You would install `Geth` on the Linux machine for CompSci590.1 running at vcm.duke.edu. See [here](https://github.com/ethereum/go-ethereum/wiki/Building-Ethereum) for more information.

***Virtual Machine***

For the class we would be using a virtual machine, where you would work on a virtual machine which have been reserved for the class. **Please use only the VM for class and not a personal Duke VCM, otherwise a security concern would be raised.**. After reserving the VM, login into it by using either the user vcm, or your duke netID. **DO NOT DELETE THE VIRTUAL MACHINE FOR ANY PURPOSE**

***Ubuntu Users***

Follow the following instructions to install `Geth` for Ubuntu.

Even when you are not using Ubuntu as native OS, you can set up VirtualBox and download a Ubuntu-1604-LTS image from [[here](http://releases.ubuntu.com/16.04.5/)]. This image is typically 1 GB, much smaller than the 7 GB image above. To set up an empty Ubuntu OS on VirtualBox, here is a useful [[link](https://medium.com/@tushar0618/install-ubuntu-16-04-lts-on-virtual-box-desktop-version-30dc6f1958d0)].


**_Script 1 Linux_**: 

```
sudo apt-get install software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install ethereum
sudo apt install screen
```

### 2. Join the Blockchain network

**2.1 Connect to the Blockchain Gateway (Bootnode)**: Every blockchain starts with the genesis block. When you run geth with default settings for the first time, the main net genesis block is committed to the database. For a private network, you usually want a different genesis block. We have a pre-defined custom [[genesis.json](genesis.json)] file. The `config` section ensures that certain protocol upgrades are immediately available. The `alloc` section pre-funds accounts, which is currently empty. Following the instructions below to run geth. A cheat sheet for screen commands: https://kapeli.com/cheat_sheets/screen.docset/Contents/Resources/Documents/index

**_Script 2.1_**: 

```
screen
mkdir -p ~/5901-a1/bkc_data
cd ~/5901-a1
vim genesis.json # Paste the contents of the pre-defined custom genesis.json
geth --datadir bkc_data init ~/5901-a1/genesis.json # create a database that uses this genesis block
geth --datadir bkc_data --http --bootnodes "enode://c419a04a0bb11531114b6ff7fafca5fb301ae2bbc576df279256a8a6c1cffc721341e92e5857feb690c7b82d6e48128a260a597403ee94ea3f02e1230156de8d@67.159.95.16:0?discport=8009" console  2>console.log


```

In the last command above, `--networkid` specify the private network ID. Connections between nodes are valid only if peers have identical protocol version and network ID, you can effectively isolate your network by setting either of these to a non default value. `--bootnode` option specify the bootnode address, following the format `[nodeID]:IP:port`. Every subsequent Geth node pointed to the bootnode for peer discovery. [This page](https://github.com/ethereum/go-ethereum/wiki/Command-Line-Options) describes the options for ```geth``` commands. The given bootnode is a private bootnode provided for the peer discovery for the class.  **DO NOT RUN THE CODE WITHOUT THE BOOTNODE OPTION OR NETWORK ID**

Check the connectivity by running:

**_Script 2.2_**: 

```
> admin.peers
```

This command should return non-empty text about the peer nodes connected by your node. If it is empty please come to office hours. You would sometimes need to wait for a while to connect to some nodes. If it is still empty use the following:
```
> admin.addPeer("enode://d17b2d0d6a8b6249e4464e63909c0369febdab85ad38ffc19bbad699e8456761b9a7a87640a967237dee133f63ac8bc37a63ccbc3b3e3ed8bb7fb7efe9b971e5@152.3.77.39:30303")
> admin.addPeer("enode://eb9c27f2b43e9799d00abe87168bfc6049e16f312adffacc95213d9cf4ca1837c14c0e2eb853fd62249875ec0732bc745603cc4c1fa56baec0d7b9caa6e24a1e@152.3.53.29:30303")
```


### 3A. Get Coins by Mining

Before mining, the coinbase has to be specified to one personal account, where your earnings will be settled. Run following commands to create a new account, and set it as coinbase.

**_Script 3a.1_**: 

```
> personal.newAccount() # create an Account
> eth.accounts # check accounts
> miner.setEtherbase(eth.accounts[0]) # Use the address that you did not register in the form, i.e. the one created above.
```

You can now start/stop the miner. If mining doesn't seem to work, you may try to unlock your account and start mining again:

```
> personal.unlockAccount(“accountAddress”, “accountPassword”)
> personal.unlockAccount(“accountAddress”, “accountPassword”, expiration_time)
```

The accountPassword is the passphrase used to generate accountAddress in Script 3b.1.

**_Script 3a.2_**: 

```
> miner.start(1)# Do not use more than one thread
> miner.stop()
```

Mining takes a while to get started, you can monitor the event log by

**_Script 3a.3_**: 

```
tail -f ~/lab1/console.log
```

To know currently you are mining or not, you can run 

**_Script 3a.4_**: 

```
> eth.hashrate # get the current mining power
> eth.blockNumber # current top block.
```

If you find your account has non-zero balance, you get some coins through mining:

**_Script 3a.5_**: 

```
> web3.fromWei(eth.getBalance(eth.accounts[0]),"ether")
```

The list of `Geth` commands can be found on [[this page](https://github.com/ethereum/go-ethereum/wiki/Management-APIs)].


### 3B. Transactions

**_Script 3b.1_**: 


```
> personal.newAccount() # create an Account
> eth.accounts # check accounts
```

Create two accounts and send amount x = DUKE_UNIQUE_ID%10 from one account to another. Attach a screenshot for the same.

**_Script 3b.2_**: 

```
> eth.sendTransaction({from:eth.accounts[0], to: eth.accounts[1], value:web3.toWei(x,"ether")})
# Once the transactions is successful
> web3.fromWei(eth.getBalance(eth.accounts[0]),"ether")
```


The list of `Geth` commands can be found on [[this page](https://github.com/ethereum/go-ethereum/wiki/Management-APIs)].


Lab Tasks
---

The tasks in this lab require to inspect and modify the content of Blockchain. In addition to the Ethereum commands you used  above, there are other relevant commands as below.

```
> eth.accounts[0] # check the account id
> eth.getBalance(<account>) # check the balance for one account, the argument is account id
> web3.fromWei(<value>,"ether") # convert Wei to Ether
> web3.toWei(<value>,"ether") #convert Ether to Wei
> eth.blockNumber # check the latest block number on the chain
> eth.getBlock(eth.blockNumber-3) # display a certain block 
> eth.getBlock('latest', true) # display the latest block
> eth.getBlock('pending', true) # display the pending block
> eth.sendTransaction({from:eth.accounts[0], to: <TA_address>, value:web3.toWei(1,"ether")})
#eth.sendTransaction({from:senderAccount, to:receiverAccount, value: <amount_you_received_from_TA>})
> eth.getTransaction(<Transaction_ID>)
> loadScript("/path/to/script.js") #run script.js (all the commands there) inside the geth console.
```

**Task 1:** After you started mining, show the coins that you have mined. Then wait 5 minutes, check the balance again.

**Task 2:** Show the latest five blocks and the latest two transaction confirmed in the blockchain. Then wait 1 minute, show the blockchain again and see how if it is extended by new blocks over time. (Hint: To find the latest transaction on the blockchain, you can iterate through the blocks and check the transaction(s) in them. To iterate through the blocks, you can use JavaScript. Alternatively, you can use a python script. You can search for the details for the same.)

**Task 3:** 

1. Submit a transaction, say `tx1`, to the blockchain. You need to send the transaction to the TA address, which you would derive from the transaction the TA sends you the coins from. Ensure you send this transaction from the registered account.

2. Show whether transaction `tx1` is included in the Blockchain; if not wait for a while, check again.

Note 1: The above command will return a hash tag which served as the ID of the transaction, you could use that ID to query the transaction in the future.

Note 2: [Ether](http://www.ethdocs.org/en/latest/ether.html) is the name of the currency used within Ethereum. Wei is the smallest unit in Ethereum. 1 Ether = 10^18 Wei. The account balance and transfer amount are shown in Wei. You can use the converter utility web3.fromWei and web3.toWei to convert between Ether and Wei. 


Deliverable
---

- For each task, you should submit the following:
    1. The script program consisting of the Geth commands (Each task should have its own task_i.js file and it should run with the loadScript command)
    2. The screenshot that shows your script has run successfully on your computer
        - Make sure include your name in the screenshot. For instance, you can open a text editor and type your name in it.

FAQ/Trouble shooting
---

- Q1: When sending transaction, I got this error: "account is locked"
   - Answer: Before sending transactions, you may need to unlock your personal wallet/account and input passphrase. Example:

```
personal.unlockAccount(eth.accounts[0])
```
- Q2: When my terminal crashes in VM (e.g., during mining),  I cannot restart the `geth` properly.
    - Answer: You can restart the VM to get around this issue. (Terminal crash may mess up network stack in your VM which `geth` depends on).
- Q3: How to check if my node is mining?
    - Answer: You can check by running "eth.mining", it returns "true" or "false" to indicate if the mining is on-going or not. Note: Command `eth.hashrate` may return "0" even if the mining process is active. 
    
             
